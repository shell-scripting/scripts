#!/bin/bash
#
# Author: Gabriel Mercado
# Email: mercadogab@gmail.com
# Date: 13122020
#
# WHO version 1.0
clear
conectados=$(who)
echo "Estos son los usuarios que se conectaron:"
echo "" 
echo $conectados
echo ""
finger -l


exit 0
