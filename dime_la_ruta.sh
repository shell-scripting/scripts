#!/bin/bash

clear

# ${var#patron} "Borra la parte más pequeña si coincide"
# ${var##patron} "Borra la parte más grande que coincide"
# ${var%patron}
# ${var%%patron}
ruta=${1:?'Falta la ruta'}

echo ${ruta##/*/}
echo ${ruta#/*/}
echo ${ruta}
echo ${ruta%.*}
echo ${ruta%%.*}

exit 0;
