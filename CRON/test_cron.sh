#!/bin/bash
#
# Author: Gabriel Mercado
# Email: mercadogab@gmail.com	
# Date: 12122020
#
# * * * * * user  command to be executed
# day of week (0 - 6) (0 is Sunday, or use names)
# month (1 - 12)
# day of month (1 - 31)
# hour (0 - 23)
# min (0 - 59)
lspci > /home/$USER/test 

exit 0
