#!/bin/bash
#
# Author: Gabriel Mercado
# Email: mercadogab@gmail.com
# Date: 12122020
#
clear
if [ $USER = 'root' ] ; then
echo "deb http://deb.debian.org/debian buster-backports main contrib non-free" >> /etc/apt/sources.list;
echo ""
$(apt-get update)
echo ""
$(apt -t buster-backports install cockpit)
echo "Done!"
else 
echo "No tienes permisos de superusuario.-"
$(exit)
fi
exit 0;
